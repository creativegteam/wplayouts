## **WP Patcher**

### **Install & activate**

1. Go to "**Plugins > Add New**".
2. Click on "**Upload Plugin**".
3.  Click on "**Browse**", locate in your system and select "**WpPatcher**" zip file.
4.  Click on "**Install Now**".
5.  When installation complete, click on "**Activate Plugin**".


For further information about WordPress plugins installation read in official [WordPress documentation](https://codex.wordpress.org/Managing_Plugins#Installing_Plugins).


***